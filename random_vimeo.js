(function($){

	$.randomVimeo = function(videoContainerID, videoWidth, videoHeight){
		$.randomVimeo.vars.videoContainerID = videoContainerID;
		$.randomVimeo.vars.videoContainerIDString = "#" + videoContainerID;
		$.randomVimeo.vars.videoContainer = $($.randomVimeo.vars.videoContainerIDString);
		$.randomVimeo.vars.height = videoHeight;
		if(videoWidth != null || videoWidth != undefined){
			$.randomVimeo.vars.width = videoWidth;
		}
		if(videoHeight != null || videoHeight != undefined){
			$.randomVimeo.vars.height = videoHeight;
		}
		$.randomVimeo.init();
	};

	$.randomVimeo.vars = {
		videoContainerID: null,
		videoContainerIDString: null,
		videoContainer: null,
		baseEmbedURL: 'https://player.vimeo.com/video/',
		videoIDList: null,
		width: 1920,
		height: 1080,
	};

	$.randomVimeo.init = function(){
		var videoString = $.randomVimeo.vars.videoContainer.data("videos");
		$.randomVimeo.vars.videoIDList = videoString.split(",");
		var arrayLength = $.randomVimeo.vars.videoIDList.length;
		var videoToSelect = Math.floor(Math.random() * (arrayLength - 1)) + 1;
		$.randomVimeo.embedVideo($.randomVimeo.vars.videoIDList[videoToSelect]);

	};

	$.randomVimeo.embedVideo = function(videoID){
		var constructedURL = $.randomVimeo.vars.baseEmbedURL + videoID;
		var htmlToEmbed = '<iframe src="' + constructedURL + '" width="'+ $.randomVimeo.vars.width +'" height="'+ $.randomVimeo.vars.height +'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		$.randomVimeo.vars.videoContainer.html(htmlToEmbed);
	};

})(jQuery);